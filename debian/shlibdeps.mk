# This makefile compiles a dummy executable linked against
# those shared libraries that are dlopen'd by Julia modules
# in Base. The dummy executable is passed to dpkg-shlibdeps
# to generate library dependencies for the julia package.
#
# The dummy executable is further used to generate symbolic
# links lib*.so => lib*.so.SOVERSION in the private library
# path for Julia, which ensures that ccall() loads exactly
# those library versions that the julia package was built
# with and depends on.

# Include Julia makefile with LIB*NAME definitions.
JULIAHOME := $(CURDIR)
include Make.inc

TARGETS := debian/shlibdeps debian/libjulia$(SOMAJOR).links

all: $(TARGETS)

ifeq ($(USE_SYSTEM_BLAS),1)
SHLIBDEPS += $(LIBBLAS)
# OpenBLAS library provides both BLAS and LAPACK.
ifneq ($(LIBBLASNAME),$(LIBLAPACKNAME))
SHLIBDEPS += $(LIBLAPACK)
endif
endif

ifeq ($(USE_SYSTEM_CURL),1)
SHLIBDEPS += -lcurl
endif

ifeq ($(USE_SYSTEM_DSFMT),1)
SHLIBDEPS += -ldSFMT
endif

ifeq ($(USE_SYSTEM_GMP),1)
SHLIBDEPS += -lgmp
endif

ifeq ($(USE_SYSTEM_LIBGIT2),1)
SHLIBDEPS += -lgit2
endif

ifeq ($(USE_SYSTEM_LIBM),1)
SHLIBDEPS += $(LIBM)
else ifeq ($(USE_SYSTEM_OPENLIBM),1)
SHLIBDEPS += $(LIBM)
endif

ifeq ($(USE_SYSTEM_LIBSSH2),1)
SHLIBDEPS += -lssh2
endif

ifeq ($(USE_SYSTEM_LIBUNWIND),1)
ifeq (,$(filter $(DEB_HOST_ARCH),s390x))
SHLIBDEPS += -lunwind
endif
endif

ifeq ($(USE_SYSTEM_LIBUV),1)
SHLIBDEPS += -luv
endif

ifeq ($(USE_SYSTEM_LLVM),1)
SHLIBDEPS += -lLLVM -L/usr/lib/llvm-$(LLVM_VER)/lib/
endif

ifeq ($(USE_SYSTEM_MBEDTLS),1)
SHLIBDEPS += -lmbedtls -lmbedcrypto -lmbedx509
endif

ifeq ($(USE_SYSTEM_MPFR),1)
SHLIBDEPS += -lmpfr
endif

ifeq ($(USE_SYSTEM_PCRE),1)
SHLIBDEPS += -lpcre2-8
endif

ifeq ($(USE_SYSTEM_SUITESPARSE),1)
SHLIBDEPS += -lcholmod -lspqr -lsuitesparseconfig -lumfpack
endif

ifeq ($(USE_SYSTEM_UTF8PROC),1)
SHLIBDEPS += -lutf8proc
endif

# The dummy executable is linked with --no-as-needed to prevent
# the linker from potentially disregarding the given libraries
# because none of the library symbols are used at compile time.
debian/shlibdeps: debian/shlibdeps.c
	$(CC) -fPIE -o $@ $< -ldl -Wl,--no-as-needed $(SHLIBDEPS)

# The soname for each library is looked up by invoking the
# dummy executable with the name of an arbitrary symbol such
# as a function exported by that library. Ideally, these
# should be functions that are ccall'd by the Julia modules.
debian/libjulia$(SOMAJOR).links: debian/shlibdeps
	rm -f $@.tmp
ifeq ($(USE_SYSTEM_BLAS),1)
	debian/shlibdeps daxpy_ >> $@.tmp
	echo " $(private_libdir)/$(LIBBLASNAME).so" >> $@.tmp
ifneq ($(LIBBLASNAME),$(LIBLAPACKNAME))
	debian/shlibdeps dggsvd_ >> $@.tmp
	echo " $(private_libdir)/$(LIBLAPACKNAME).so" >> $@.tmp
endif
endif
ifeq ($(USE_SYSTEM_CURL),1)
	debian/shlibdeps curl_easy_recv >> $@.tmp
	echo " $(private_libdir)/libcurl.so" >> $@.tmp
endif
ifeq ($(USE_SYSTEM_DSFMT),1)
	debian/shlibdeps dsfmt_get_idstring >> $@.tmp
	echo " $(private_libdir)/libdSFMT.so" >> $@.tmp
endif
ifeq ($(USE_SYSTEM_GMP),1)
	debian/shlibdeps __gmpz_get_str >> $@.tmp
	echo " $(private_libdir)/libgmp.so" >> $@.tmp
endif
ifeq ($(USE_SYSTEM_LIBGIT2),1)
	debian/shlibdeps git_libgit2_version >> $@.tmp
	echo " $(private_libdir)/libgit2.so" >> $@.tmp
endif
ifeq ($(USE_SYSTEM_LIBM),1)
	debian/shlibdeps pow >> $@.tmp
	echo " $(private_libdir)/$(LIBMNAME).so" >> $@.tmp
else ifeq ($(USE_SYSTEM_OPENLIBM),1)
	debian/shlibdeps pow >> $@.tmp
	echo " $(private_libdir)/$(LIBMNAME).so" >> $@.tmp
endif
ifeq ($(USE_SYSTEM_MPFR),1)
	debian/shlibdeps mpfr_init2 >> $@.tmp
	echo " $(private_libdir)/libmpfr.so" >> $@.tmp
endif
ifeq ($(USE_SYSTEM_PCRE),1)
	debian/shlibdeps pcre2_compile_8 >> $@.tmp
	echo " $(private_libdir)/libpcre2-8.so" >> $@.tmp
endif
ifeq ($(USE_SYSTEM_LIBSSH2),1)
	debian/shlibdeps libssh2_version >> $@.tmp
	echo " $(private_libdir)/libssh2.so" >> $@.tmp
endif
ifeq ($(USE_SYSTEM_LIBUV),1)
	debian/shlibdeps uv_tcp_open >> $@.tmp
	echo " $(private_libdir)/libuv.so" >> $@.tmp
endif
ifeq ($(USE_SYSTEM_LIBUNWIND),1)
	debian/shlibdeps backtrace >> $@.tmp
	echo " $(private_libdir)/libunwind.so" >> $@.tmp
endif
ifeq ($(USE_SYSTEM_LLVM),1)
	debian/shlibdeps llvm_regexec >> $@.tmp
	echo " $(private_libdir)/libLLVM.so" >> $@.tmp
endif
ifeq ($(USE_SYSTEM_MBEDTLS),1)
	debian/shlibdeps mbedtls_net_init >> $@.tmp
	echo " $(private_libdir)/libmbedtls.so" >> $@.tmp
	debian/shlibdeps mbedtls_md5 >> $@.tmp
	echo " $(private_libdir)/libmbedcrypto.so" >> $@.tmp
	debian/shlibdeps mbedtls_x509_get_serial >> $@.tmp
	echo " $(private_libdir)/libmbedx509.so" >> $@.tmp
endif
ifeq ($(USE_SYSTEM_SUITESPARSE),1)
	debian/shlibdeps cholmod_version >> $@.tmp
	echo " $(private_libdir)/libcholmod.so" >> $@.tmp
	debian/shlibdeps SuiteSparseQR_C_free >> $@.tmp
	echo " $(private_libdir)/libspqr.so" >> $@.tmp
	debian/shlibdeps SuiteSparse_config >> $@.tmp
	echo " $(private_libdir)/libsuitesparseconfig.so" >> $@.tmp
	debian/shlibdeps umfpack_dl_report_info >> $@.tmp
	echo " $(private_libdir)/libumfpack.so" >> $@.tmp
endif
ifeq ($(USE_SYSTEM_UTF8PROC),1)
	debian/shlibdeps utf8proc_errmsg >> $@.tmp
	echo " $(private_libdir)/libutf8proc.so" >> $@.tmp
endif
	mv $@.tmp $@

clean:
	$(RM) $(TARGETS)
