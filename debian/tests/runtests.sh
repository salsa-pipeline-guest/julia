#!/bin/sh
set -ex

export JULIA_TEST_MAXRSS_MB=500
export HOME=/tmp

cd $AUTOPKGTEST_TMP

/usr/bin/julia \
	--check-bounds=yes \
	--startup-file=no \
	/usr/share/julia/test/runtests.jl all
